#include <cstdlib> 
#include <iostream>
#include <vector>
#include <set>
#include <chrono>

using namespace std;
using namespace std::chrono;


typedef struct vertice {
    int cor;    // Cor do vertice
    vector<int> adjacentes; // Lista de adjacentes
} Vertice;


typedef vector<Vertice> Grafo;  // O grafo sera um vetor, onde cada posicao i é a tupla (cor, lista_de_vizinhos) do vertice i + 1

set<int> adj_pivo; //conjunto dos adjacentes ao conjunto pivo
set<int> adj_aux;   //conjunto auxiliar dos adjacentes ao conjunto pivo
set<int> vertices_do_pivo; //conjunto com os vertices pertencentes ao pivo


int n;      // Numero de vertices
int m;      // Numero de arestas
int c;      // Numero de cores
int v1, v2; // Variaveis auxiliares para entrada de vértices ligados por aresta

int pivo_index;   // Indice do primeiro vertice pertencente ao pivo
Vertice pivo; //Primeiro vertice pertencente ao pivo
int passos; //Numero de passos necessários para terminar o jogo
    
Vertice aux;    // Vertice auxiliar para entrada da cor via teclado
int prox_cor = 0;  //Indice da próxima cor selecionada

Grafo grafo;    // Grafo do jogo

void imprimirResultado(); //imprime o resultado
void agrupamento(); //agrupa vertices adjacentes com a mesma cor
int proximaCor();   //verifica qual a próxima cor selecionada
void colore(int x); //colore os vertices do conjunto do pivo com a nova cor

int main(){
    
    cin >> n >> m >> c >> pivo_index;     // Lendo a primeira linha do arquivo de entrada
    
    for(int i = 0; i < n; i++){     // Lendo as cores dos vertices       
        cin >> aux.cor;
        grafo.push_back(aux);
    }
    
    
    for(int i = 0; i < m; i++){     // Lendo as adjacencias dos vertices
        cin >> v1 >> v2;
        grafo[v1-1].adjacentes.push_back(v2-1);
        grafo[v2-1].adjacentes.push_back(v1-1);   // Grafo nao direcionado, deve-se adicionar v1 -> v2 e v2 -> v1
    }

    high_resolution_clock::time_point t1 = high_resolution_clock::now(); 

    pivo_index = pivo_index - 1; //armazenando o index do pivo
    pivo = grafo[pivo_index]; //armazenando pivo

    adj_pivo.insert(pivo.adjacentes.begin(), pivo.adjacentes.end()); //adicionando adjacentes do pivo no conjunto de adjacentes do pivo
    vertices_do_pivo.insert(pivo_index); //adicionando pivo ao conjunto de vertices do pivo
      
    //agrupando o pivo com seus adjacentes de mesma cor
    agrupamento();

    //enquanto o conjunto de adjacentes do pivo estiver vazio
    while (!adj_pivo.empty()) {
        passos++;
        // cout << "Proxima cor: ";
        int prox_cor = proximaCor(); //decide proxima cor
        // cout<<prox_cor<<endl<<endl;
        pivo.cor = prox_cor; //define a cor do pivo como nova cor
        colore(prox_cor); //colore todos os grafos do vertices_do_pivo com a nova cor
        agrupamento(); //reagrupa utilizando a nova cor  
        imprimirResultado(); 
    }

    imprimirResultado(); 

    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

    cout<<"Tempo:"<<"\t"<<time_span.count()<<" segundos"<<endl;
    cout<<"Passos:"<<" "<<passos<<endl;
    return 0;
    
}

void imprimirResultado(){
    // for(int i = 0; i < n; i++){
        // cout << "Sou o vertice " << i+1 << " de cor " << grafo[i].cor << endl;
        /*cout << "Meus vizinhos sao" << endl;
        for(auto vizinho : grafo[i].adjacentes) {
            cout << "\t v: "<< vizinho + 1 << endl;
        }*/
    // }

    cout <<endl<< "Conjunto do pivo:"<<endl;
    cout << "\t";
    for (auto v : vertices_do_pivo) {
        cout << v + 1<<" ";
    }
    cout << endl;
    cout << "Adjacentes ao pivo:"<<endl;
    cout << "\t";
    for (auto v : adj_pivo) {
        cout << v + 1<<" ";
    }
    cout << endl;
}

void agrupamento() {
    adj_aux = set<int>(); //aux vazio
    bool achou_novos_vizinhos = false;

    for (auto v : adj_pivo) {
        //percorrendo os adjacentes do pivo para ver quais são da mesma cor
        if (grafo[v].cor == pivo.cor) {
            adj_aux.insert(grafo[v].adjacentes.begin(), grafo[v].adjacentes.end());
            vertices_do_pivo.insert(v);
            //existe adjacente ao pivo da mesma cor
            achou_novos_vizinhos = true;
        }          
        else {
            adj_aux.insert(v);
        }
    }
    
    for (auto v : vertices_do_pivo) {
        //apagando vertices repetidos
        adj_aux.erase(v);
    }

    adj_pivo = adj_aux; //transfere aux pro pivo

    if (achou_novos_vizinhos) {
        //se existir adjacente ao pivo da mesma cor, continua o agrupamento
        agrupamento();
    }
}

int proximaCor() {
    int maior = 0;
    int index = 0;
    vector<int> contador_de_cores(c,0); //tamanho c (quant de cores) e inicializa todas cores com 0
    for (auto v : adj_pivo) {
        int cor = grafo[v].cor - 1;
        contador_de_cores[cor]++;
    }
    for (int i = 0; i<c; i++) {
        if (maior < contador_de_cores[i]) {
            index = i;
            maior = contador_de_cores[i];
        }
    }
    return index+1;
}

void colore(int x) {
    for (auto v : vertices_do_pivo) {
        grafo[v].cor = x;
    }
}