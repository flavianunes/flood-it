# Flood-it
Trabalho feito para disciplina de Teoria dos Grafos em conjunto com <a href="https://github.com/mayconamaro">@MayconAmaro</a>

<h1>Objetivo</h1>
Implementar um método para solução do problema de inundação em grafos (derivado de um popular jogo chamado Flood-It)
<h1>Instancias</h1>
Todas as instancias foram fornecidas pelo professor da discplina. 
<h1>Resultados</h1>
<img src="/resultados.png">
<h1>
